import logo from './logo.svg';
import './App.css';
import UserComponent from './Components/User';
import Hook from  './Components/User/Hook'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Hola mundo desde React c:
        </p>
        <p>
          Josafat Flores
        </p>
      </header>
      <UserComponent/>
      <Hook/>
    </div>
  );
}

export default App;
