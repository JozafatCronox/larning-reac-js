import { Component } from 'react';
import Profile from './Profile'

class UserComponent extends Component {

    constructor() {
        super()
        this.state = {
            name: 'Josafat',
            lastname: 'Flores',
            age: 26,
            gender: 'Male',
            email: 'josafatfloresr@gmail.com',
            counter: 0
        }
    }
    sayHello = () => {
        console.log('Hello')
    }


    incrementCounterValue = () => {
        this.setState({
            counter: this.state.counter + 1
        })
    }

    incrementCounterValue2 = () => {
        this.setState({
            counter: this.state.counter + 2
        })
    }

    /*
    incrementCounterValue = () => {
        this.setState(prevState =>{
            console.log(prevState)
            return prevState.counter + 1
        })
    }
    */
    render() {
        const {
            name,
            lastName,
            age,
            gender,
            email,
            counter
        } = this.state

        return (
            <>
                <h1>Mi nombre es: { name } { lastName }</h1>
                <h2>Tengo { age } años</h2>
                <h2>Soy { gender }</h2>
                <h2>Mi email es { email }</h2>
                <button
                    onClick={
                        () =>
                            this.incrementCounterValue()
                    }
                >
                    Presiona para incrementar
                </button>
                <p>Contador: { counter }</p>
                <Profile
                    name = { name }
                    lastname = { lastName }
                    email = { email }
                    counter = { counter }
                    increment2 = { this.incrementCounterValue2 }
                />
            </>
        )
    }
}

export default UserComponent;