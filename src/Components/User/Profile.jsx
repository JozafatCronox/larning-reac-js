import React from 'react'
//import Image from './Image'


const Profile = (props) => {
    return (
        <div>
            <h1>Componente de perfil</h1>
            <h3>Nombre: { props.name }</h3>
            <h3>Apellido: { props.lastName }</h3>
            <h3>Email: { props.email }</h3>
            <h3>Contador: { props.counter }</h3>
            <button onClick={ props.increment2 }>Incrementa el contador 2</button>
            {/*<Image />*/}
        </div>
    )
}
/*
function Profile2(){
    return(
        <h1>Soy un conponente de funcion ES5</h1>
    )
}
*/

export default Profile
